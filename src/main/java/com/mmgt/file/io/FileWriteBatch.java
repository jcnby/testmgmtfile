package com.mmgt.file.io;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;

public class FileWriteBatch {

	private String fileName;
	private ReentrantLock locker;
	private SimpleDateFormat formatter;
	
	public FileWriteBatch(String fileName, ReentrantLock lock, SimpleDateFormat formatter) {
		
		this.fileName = fileName;
		this.locker = lock;
		this.formatter = formatter;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public ReentrantLock getLocker() {
		return locker;
	}

	public void setLocker(ReentrantLock locker) {
		this.locker = locker;
	}

	public SimpleDateFormat getFormatter() {
		return formatter;
	}

	public void setFormatter(SimpleDateFormat formatter) {
		this.formatter = formatter;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fileName == null) ? 0 : fileName.hashCode());
		result = prime * result + ((locker == null) ? 0 : locker.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FileWriteBatch other = (FileWriteBatch) obj;
		if (fileName == null) {
			if (other.fileName != null)
				return false;
		} else if (!fileName.equals(other.fileName))
			return false;
		if (locker == null) {
			if (other.locker != null)
				return false;
		} else if (!locker.equals(other.locker))
			return false;
		return true;
	}

	public void addToFile(String text) {
		
		locker.lock();
		
		try {
			Thread.currentThread().sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try{
		    FileWriter fw = new FileWriter(fileName,true); //the true will append the new data
		    fw.write(formatter.format(new Date()) + " " + text + "\n");//appends the string to the file
		    fw.close();
		}
		catch(IOException ioe){
		    System.err.println("IOException: " + ioe.getMessage());
		}finally{
            locker.unlock(); // снимаем блокировку
        }
	}
}
