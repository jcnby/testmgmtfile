package com.mmgt.file;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.mmgt.file.io.ChangeFileService;

@SpringBootApplication
public class TestMgmtFileApplication {

	@Autowired
	ChangeFileService changeFileService;
	
	public static void main(String[] args) {
		SpringApplication.run(TestMgmtFileApplication.class, args);
	}

}
