package com.mmgt.file.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mmgt.file.io.ChangeFileService;

@RestController
public class FileChangeController {
	
	@Autowired
	ChangeFileService changeFileService;
	
	@RequestMapping(value= "/addTiFile", method = RequestMethod.GET)
	public ResponseEntity setCommand(
			@DefaultValue("") @RequestParam(required = true) String file,
			@DefaultValue("") @RequestParam(required = true) String string) {
		
		if(file == null || file.isEmpty()) {
			return new ResponseEntity<>("invalid file param", HttpStatus.BAD_REQUEST);
		}
		changeFileService.addChangeEvent(file, string);
		return new ResponseEntity<>("", HttpStatus.OK);
	}
	
}
