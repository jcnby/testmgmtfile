package com.mmgt.file.io;

import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.stereotype.Service;

@Service
public class ChangeFileService {
	
	private Map<String, FileWriteBatch> files = new ConcurrentHashMap<>();
	
	private ReentrantLock locker = new ReentrantLock();
	
	private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
	
	
	public void addChangeEvent(String file, String txt) {
		
		// lock while new FileWriteBatch creating
		locker.lock();
		FileWriteBatch batch = files.get(file);
		if(batch == null) {
			files.put(file, new FileWriteBatch(file, new ReentrantLock(), formatter));
			batch = files.get(file);
		}
		locker.unlock();
		
		batch.addToFile(txt);
		
	}
	
}
